#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<malloc.h>

typedef struct binary_tree
{
    struct binary_tree *left_sub_tree;
    int val;
    struct binary_tree *right_sub_tree;
} tree;

tree *root;

tree *insert(tree *r, tree *n)
{
    if(r == NULL){
        r = n;
        r->left_sub_tree = NULL;
        r->right_sub_tree = NULL;
        return;
    }

    else if(n->val < r->val)
        r->left_sub_tree = insert(r->left_sub_tree,n);
    else if(n->val > r->val)
        r->right_sub_tree = insert(r->right_sub_tree,n);

    else printf("\n\nError!!!");
    return r;
}


tree *inorder(tree *r)
{
    if(r){
        inorder(r->left_sub_tree);
        printf("\n\t %d ", r->val);
        inorder(r->right_sub_tree);
    }
    return 0;
}

tree *preorder(tree *r)
{
    if(r){
        printf("\n\t %d ", r->val);
        preorder(r->left_sub_tree);
        preorder(r->right_sub_tree);
    }
    return 0;
}

tree *postorder(tree *r)
{
    if(r){
        preorder(r->left_sub_tree);
        preorder(r->right_sub_tree);
        printf("\n\t %d ", r->val);
    }
    return 0;
}


void display()
{
    printf("\n\n\tThis is inorder display\n");
    inorder(root);
    getch();

    printf("\n\n\tThis is preorder display\n");
    preorder(root);
    getch();

    printf("\n\n\tThis is postrder display\n");
    postorder(root);
    getch();

}

void main()
{
    int ch, s_key, d_key;
    tree *ptr;

    for(;;){
        printf("\n1.Insert\n2.Display\n");
        scanf("%d",&ch);

        switch(ch)
        {
            case 1:
                ptr = (struct binary_tree*)malloc(sizeof(struct binary_tree));
                printf("Enter : ");
                scanf("%d", &ptr->val);
                ptr->left_sub_tree = ptr->right_sub_tree = NULL;
                root = insert(root,ptr);
                break;

            case 2:
                display();
                break;

            case 3:
                exit(0);


        }
    }
}
